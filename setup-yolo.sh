#! /usr/bin/env bash


cd yolo/

conda env create -f conda-env.yml

conda activate yolo
pip install requests
python download-and-convert-weights.py
python download-weights.py
python train.py
python predict.py
echo "After predictions the results are stored in the yolo/results/ directory"
cd -

conda deactivate
