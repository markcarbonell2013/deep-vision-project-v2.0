import os
import subprocess
import time
import sys


def make_call_string(arglist):
    result_string = ""
    for arg in arglist:
        result_string += "".join(["--", arg[0], " ", arg[1], " "])
    return result_string


root_folder = os.path.dirname(os.path.abspath(__file__))
model_folder = os.path.join(root_folder, "pretrained-weights")
image_folder = os.path.join(root_folder, "dataset", "PennFudanPed")
input_folder = os.path.join(image_folder, "PNGImages")
output_folder = os.path.join(root_folder, "results")


if not os.path.exists(output_folder):
    os.mkdir(output_folder)

# First download the pre-trained weights
download_script = "./download-yolo-weights.py" # os.path.join(model_folder, "Download_Weights.py")

if not os.path.isfile(os.path.join(model_folder, "trained_weights_final.h5")):
    print("\n", "Downloading Pretrained Weights", "\n")
    start = time.time()
    call_string = " ".join(
        [
            "python",
            download_script,
            "1MGXAP_XD_w4OExPP10UHsejWrMww8Tu7",
            os.path.join(model_folder, "trained_weights_final.h5"),
        ]
    )

    subprocess.call(call_string, shell=True)

    end = time.time()
    print("Downloaded Pretrained Weights in {0:.1f} seconds".format(end - start), "\n")
