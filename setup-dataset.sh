#! /usr/bin/env bash

# This script will download all the PennFudanPed dataset

DIR="$(cd "$(dirname "$0")" && pwd)"
#if [ ! -d $DIR/dataset/PennFudanPed ]; then
    wget https://www.cis.upenn.edu/~jshi/ped_html/PennFudanPed.zip
    mv PennFudanPed.zip ./dataset/
    cd ./dataset
    unzip -o PennFudanPed.zip
    mkdir -p PennFudanPed/TestImages
    rm PennFudanPed.zip
    cd -

    wget "https://images.unsplash.com/photo-1517732306149-e8f829eb588a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://cdn.theatlantic.com/static/mt/assets/food/RTR2LP34edit.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://exchange.aaa.com/wp-content/uploads/2014/03/Pedestrian-Safety.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://www.liberaldictionary.com/wp-content/uploads/2018/12/pedestrian.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://www.hcalawyers.com.au/wp-content/uploads/2017/05/people-using-crosswalk-1024x569.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://www.thefloridanewsjournal.com/wp-content/uploads/2018/07/Florida-Cities-Ranked-Most-Dangerous-For-Pedestrians...-Again.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://thenewswheel.com/wp-content/uploads/2018/04/pexels-photo-109919-760x428.jpeg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://exchange.aaa.com/wp-content/uploads/2014/03/Pedestrians-Walking-Crosswalk1.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://media.nj.com/ledgerupdates_impact/photo/2012/07/nj-walk-onejpg-b6f9e01b09fdca5f.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://www.wired.com/images_blogs/autopia/2009/11/crosswalk.jpg" -P $DIR/dataset/PennFudanPed/TestImages
    wget "https://www.rnz.de/cms_media/module_img/601/300769_2_org_Untere_Strasse_Sperrzeiten_Heidelberg_Partymeile_-_neutrale_BU_5a9ef682745a9.jpg" -P $DIR/dataset/PennFudanPed/TestImages

#fi

which sed || sudo apt install sed

BASE_DIR="./dataset/PennFudanPed"

echo "$BASE_DIR"

if [ -f "$BASE_DIR/yolo-annotations.txt" ]; then 
    rm "$BASE_DIR/yolo-annotations.txt"
fi


if [ ! -f "$BASE_DIR/yolo-classes.txt" ]; then
    echo "PASpersonWalking" > "$BASE_DIR/yolo-classes.txt"
fi

touch "$BASE_DIR/yolo-annotations.txt"
for f in $BASE_DIR/Annotation/*; do
    echo "$f"
    coords="$(grep -Eio '[0-9]+, [0-9]+.*[0-9]+, [0-9]+' "$f" | sed -E -e 's/\) - \(/,/' -e 's/\s+//g' | sed -E ':a;/$/{N;s/\n/,0 /;ba}' ),0"
    imgname="$(realpath ./dataset/$(grep -Eio 'PennFudanPed/PNGImages/.*\.png' "$f" ))"
    echo "$imgname $coords" >> "$BASE_DIR/yolo-annotations.txt"
done

cat "$BASE_DIR/yolo-annotations.txt"

