#! /usr/bin/env bash

cd mask-rcnn/
conda env create -f conda-env.yml
conda activate mask-rcnn
python trainModel.py

conda deactivate
cd -

