import tensorflow as tf

# conda install -c conda-forge tensorflow
# conda install -c conda-forge ipykernel

import numpy as np
import IPython.display as display
import os

files = []
cDir = os.getcwd()
for f in os.listdir('../dataset/training_0000'):
    if 'segment-' in f:
        files.append(os.path.join(cDir, f'../dataset/training_0000/{f}'))

rawData = tf.data.TFRecordDataset(files)


def extract(data):
    features = {
            'intList': tf.FixedLenFeature([], tf.int8)
            }


