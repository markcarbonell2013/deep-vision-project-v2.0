
# Deep Vision Project

### University of Heidelberg
### Tutor: Nikolai Ufer
### Prof: Bjoern Ommer
### Fabian Regnery & Marcos Carbonell
---

## Models

Our repository contains the code of four different CNN architectures:
- CenterNet [https://github.com/Duankaiwen/CenterNet](code)
- Mask-RCNN [https://github.com/facebookresearch/Detectron](code)
- Fast-RCNN [https://github.com/facebookresearch/Detectron](code)
- YOLOv4    [https://github.com/pjreddie/darknet](code)

## Architecture

Our project base directories are:
- `centernet` contains CenterNet's code
- `fast-rcnn` contains Fast-RCNN's code (Detectron repository)
- `mask-rcnn` contains Mask-RCNN's code
- `YOLO` contains YOLOv4's code
- `dataset` contains the Waymo Open Dataset and a reader we've developed to transform the Tensorflow binaries into Tensors and JPEG images


## Setup

**NOTE:** This project was build and tested in a Linux machine running Ubuntu 20.04. In order to avoid problems with the installation of our depedencies, the computer you're downloading our project into should have: 

1. Linux operating system
2. Conda or miniconda already installed and available in PATH

We use `conda` to install all the dependencies for our projects.
Everyone of the above mentioned repositories has a `setup.sh` executable which does the following:
1. Creates a conda environment with a name and a specific python version
2. Install's the projects dependencies with the correct versions
3. Deactivates the environment afterwards

The conda environments which our setup scripts create once create are called `yolo`, `mask-rcnn` and `centernet`.

In order to run them correctly you need to use the `-i` bash flag for interactive mode. This is done so that the script can create and install the dependencies of a new conda environment
The first script to run is the `setup-dataset.sh` script. It will download and setup the Penn-Fudan Dataset. The command would look like:
```bash
cd deep-vision-project-v2.0 && \
bash -i setup-dataset.sh # this script will download the dataset and format it for it to be readable by our models 
```

The command for setting CenterNet up. It will show you the predictions of an MS-COCO pretrained model on Heidelberg images if it's executed properly.
```bash
cd deep-vision-project-v2.0 && \
bash -i setup-centernet.sh # this will download and install centernet's dependencies, and compile the coffe2 modules inside
```

The command for setting Mask-RCNN up. It will install the conda dependencies necessary to train Mask-RCNN and then will train mask-rcnn on the dataset
```bash
cd deep-vision-project-v2.0 && \
bash -i setup-mask-rcnn.sh  # this script will install mask-rcnn
```

The command for setting YOLO dependencies. It will download the weights of the YOLOv3 model, which takes about 10min, and then it will start training and making predictions with the model on our own test images automatically. The results are saved under `yolo/results`
```bash
cd deep-vision-project-v2.0 && \
bash -i setup-yolo.sh 
```














