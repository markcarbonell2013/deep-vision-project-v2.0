#! /usr/bin/env bash


set -e
which conda || (echo "Please install conda to use this package" && exit 1)

if [ -d cocoapi ]; then 
    rm -rf cocoapi
fi

if [ -z "$(conda env list | grep -i "centernet" )" ]; then
    conda create --name centernet -y python=3.6
    sudo apt install libgtk2.0-dev
    conda activate centernet
    conda install -y -c conda-forge easydict opencv=4.1.0 progress matplotlib
    conda install -y -c anaconda cython scipy
    conda install -y pytorch=0.4.1 torchvision cuda92 -c pytorch
fi

git clone https://github.com/cocodataset/cocoapi.git cocoapi
cd cocoapi/PythonAPI/
make
python setup.py install --user
cd ../../centernet

pip install -r requirements.txt

cd src/lib/models/networks/DCNv2
./make.sh
cd -

cd src/lib/external 
make
cd -

wget -L 'https://drive.google.com/u/0/uc?id=1pl_-ael8wERdUREEnaIfqOV_VF2bEVRT&export=download' -O ctdet_coco_dla_2x.pth
mv ctdet_coco_dla_2x.pth models/ctdet_coco_dla_2x.pth
python src/demo.py ctdet --demo images/img-heidelberg4.jpg --load_model models/ctdet_coco_dla_2x.pth

conda deactivate
# conda env remove --name centernet
